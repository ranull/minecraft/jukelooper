package com.ranull.jukelooper;

import com.ranull.jukelooper.command.JukeLooperCommand;
import com.ranull.jukelooper.listener.*;
import com.ranull.jukelooper.manager.LooperManager;
import org.bstats.bukkit.Metrics;
import org.bukkit.command.PluginCommand;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;

public class JukeLooper extends JavaPlugin {
    private LooperManager looperManager;

    @Override
    public void onEnable() {
        looperManager = new LooperManager(this);

        saveDefaultConfig();
        registerConfigHack();
        registerMetrics();
        registerListeners();
        registerCommands();
    }

    @Override
    public void onDisable() {
        looperManager.disable();
    }

    private void registerMetrics() {
        new Metrics(this, 12854);
    }

    public void registerConfigHack() {
        getConfig().options().pathSeparator('/');

        try {
            getConfig().load(new File(getDataFolder() + File.separator + "config.yml"));
        } catch (IOException | InvalidConfigurationException exception) {
            exception.printStackTrace();
        }
    }

    public void registerListeners() {
        getServer().getPluginManager().registerEvents(new PlayerJoinListener(this), this);
        getServer().getPluginManager().registerEvents(new ChunkLoadListener(this), this);
        getServer().getPluginManager().registerEvents(new ChunkUnloadListener(this), this);
        getServer().getPluginManager().registerEvents(new PlayerMoveListener(this), this);
        getServer().getPluginManager().registerEvents(new PlayerInteractListener(this), this);
        getServer().getPluginManager().registerEvents(new BlockBreakListener(this), this);
        getServer().getPluginManager().registerEvents(new EntityExplodeListener(this), this);
        getServer().getPluginManager().registerEvents(new BlockRedstoneListener(this), this);
        getServer().getPluginManager().registerEvents(new ItemSpawnListener(this), this);
    }

    public void registerCommands() {
        PluginCommand pluginCommand = getCommand("jukelooper");

        if (pluginCommand != null) {
            JukeLooperCommand jukeLooperCommand = new JukeLooperCommand(this);

            pluginCommand.setExecutor(jukeLooperCommand);
            pluginCommand.setTabCompleter(jukeLooperCommand);
        }
    }

    public LooperManager getLooperManager() {
        return looperManager;
    }
}
