package com.ranull.jukelooper.listener;

import com.ranull.jukelooper.JukeLooper;
import org.bukkit.block.BlockState;
import org.bukkit.block.Jukebox;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.world.ChunkLoadEvent;

public class ChunkLoadListener implements Listener {
    private final JukeLooper plugin;

    public ChunkLoadListener(JukeLooper plugin) {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onChunkLoad(ChunkLoadEvent event) {
        for (BlockState blockState : event.getChunk().getTileEntities()) {
            if (blockState instanceof Jukebox) {
                if (plugin.getLooperManager().hasLooper(blockState.getLocation())) {
                    if (!((Jukebox) blockState).isPlaying()) {
                        plugin.getLooperManager().nextDisc(plugin.getLooperManager()
                                .getLooper(blockState.getLocation()));
                    }
                } else {
                    plugin.getLooperManager().createLooper((Jukebox) blockState);
                }
            }
        }
    }
}
