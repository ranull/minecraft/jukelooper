package com.ranull.jukelooper.listener;

import com.ranull.jukelooper.JukeLooper;
import org.bukkit.block.Block;
import org.bukkit.block.Jukebox;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityExplodeEvent;

public class EntityExplodeListener implements Listener {
    private final JukeLooper plugin;

    public EntityExplodeListener(JukeLooper plugin) {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onBlockExplode(EntityExplodeEvent event) {
        for (Block block : event.blockList()) {
            if (block.getState() instanceof Jukebox && plugin.getLooperManager().hasLooper(block.getLocation())) {
                plugin.getLooperManager().removeLooper(plugin.getLooperManager().getLooper(block.getLocation()));
            }
        }
    }
}
