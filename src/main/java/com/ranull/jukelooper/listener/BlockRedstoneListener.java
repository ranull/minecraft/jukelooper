package com.ranull.jukelooper.listener;

import com.ranull.jukelooper.JukeLooper;
import com.ranull.jukelooper.looper.Looper;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.Jukebox;
import org.bukkit.block.data.Directional;
import org.bukkit.block.data.type.RedstoneWire;
import org.bukkit.block.data.type.Repeater;
import org.bukkit.block.data.type.Switch;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockRedstoneEvent;

import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;

public class BlockRedstoneListener implements Listener {
    private final JukeLooper plugin;

    public BlockRedstoneListener(JukeLooper plugin) {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onBlockRedstone(BlockRedstoneEvent event) {
        if (event.getOldCurrent() == 0) {
            Block block = event.getBlock();
            Set<BlockFace> blockFaceSet = new HashSet<>();

            if (block.getBlockData() instanceof RedstoneWire) {
                RedstoneWire redstoneWire = (RedstoneWire) block.getBlockData();

                for (BlockFace blockFace : redstoneWire.getAllowedFaces()) {
                    if (redstoneWire.getFace(blockFace) != RedstoneWire.Connection.NONE) {
                        blockFaceSet.add(blockFace);
                    }
                }
            } else if ((block.getBlockData() instanceof Switch
                    || block.getBlockData() instanceof Repeater
                    || block.getBlockData() instanceof Comparator)
                    && block.getBlockData() instanceof Directional) {
                blockFaceSet.add(((Directional) block.getBlockData()).getFacing());
            }

            for (BlockFace blockFace : blockFaceSet) {
                blockFace = blockFace.getOppositeFace();
                Block blockRelative = block.getRelative(blockFace);

                if (!(blockRelative.getState() instanceof Jukebox)) {
                    blockRelative = blockRelative.getRelative(BlockFace.UP);
                }

                if (blockRelative.getState() instanceof Jukebox
                        && plugin.getLooperManager().findStorage(blockRelative.getLocation()) != null) {
                    Looper looper = plugin.getLooperManager().getLooper(blockRelative.getLocation());

                    if (looper == null) {
                        looper = plugin.getLooperManager().createLooper((Jukebox) blockRelative.getState());
                    }

                    plugin.getLooperManager().nextDisc(looper);
                }
            }
        }
    }
}
