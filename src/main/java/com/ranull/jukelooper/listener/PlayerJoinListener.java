package com.ranull.jukelooper.listener;

import com.ranull.jukelooper.JukeLooper;
import org.bukkit.block.BlockState;
import org.bukkit.block.Jukebox;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerJoinListener implements Listener {
    private final JukeLooper plugin;

    public PlayerJoinListener(JukeLooper plugin) {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerJoin(PlayerJoinEvent event) {
        for (BlockState blockState : event.getPlayer().getLocation().getChunk().getTileEntities()) {
            if (blockState instanceof Jukebox && !plugin.getLooperManager().hasLooper(blockState.getLocation())) {
                plugin.getLooperManager().createLooper((Jukebox) blockState);
            }
        }
    }
}
