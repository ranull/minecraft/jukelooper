package com.ranull.jukelooper.listener;

import com.ranull.jukelooper.JukeLooper;
import org.bukkit.block.BlockState;
import org.bukkit.block.Jukebox;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.world.ChunkUnloadEvent;

public class ChunkUnloadListener implements Listener {
    private final JukeLooper plugin;

    public ChunkUnloadListener(JukeLooper plugin) {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onChunkUnload(ChunkUnloadEvent event) {
        for (BlockState blockState : event.getChunk().getTileEntities()) {
            if (blockState instanceof Jukebox && plugin.getLooperManager().hasLooper(blockState.getLocation())) {
                Jukebox jukebox = (Jukebox) blockState;

                jukebox.setPlaying(null);
                jukebox.update(true, true);
                plugin.getLooperManager().removeLooper(plugin.getLooperManager().getLooper(blockState.getLocation()));
            }
        }
    }
}
