package com.ranull.jukelooper.listener;

import com.ranull.jukelooper.JukeLooper;
import com.ranull.jukelooper.looper.Looper;
import org.bukkit.block.Jukebox;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

public class PlayerInteractListener implements Listener {
    private final JukeLooper plugin;

    public PlayerInteractListener(JukeLooper plugin) {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onJukeboxInteract(PlayerInteractEvent event) {
        if (event.getAction() == Action.RIGHT_CLICK_BLOCK && event.getClickedBlock() != null
                && event.getClickedBlock().getState() instanceof Jukebox) {
            Player player = event.getPlayer();
            ItemStack itemInMainHand = player.getInventory().getItemInMainHand();
            Jukebox jukebox = (Jukebox) event.getClickedBlock().getState();
            Looper looper = plugin.getLooperManager().getLooper(jukebox.getLocation());

            if (itemInMainHand.getType().isRecord() && plugin.getLooperManager()
                    .findStorage(jukebox.getLocation()) != null) {
                if (looper == null) {
                    looper = plugin.getLooperManager().createLooper(jukebox);
                }

                plugin.getLooperManager().updateLooper(looper);
            }
        }
    }
}
