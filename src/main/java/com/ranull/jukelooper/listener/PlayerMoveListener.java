package com.ranull.jukelooper.listener;

import com.ranull.jukelooper.JukeLooper;
import org.bukkit.block.BlockState;
import org.bukkit.block.Jukebox;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

public class PlayerMoveListener implements Listener {
    private final JukeLooper plugin;

    public PlayerMoveListener(JukeLooper plugin) {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerMove(PlayerMoveEvent event) {
        if (event.getTo() != null && event.getTo().getChunk() != event.getFrom().getChunk()) {
            for (BlockState blockState : event.getTo().getChunk().getTileEntities()) {
                if (blockState instanceof Jukebox) {
                    if (plugin.getLooperManager().hasLooper(blockState.getLocation())) {
                        if (!((Jukebox) blockState).isPlaying()) {
                            plugin.getLooperManager().nextDisc(plugin.getLooperManager()
                                    .getLooper(blockState.getLocation()));
                        }
                    } else {
                        plugin.getLooperManager().createLooper((Jukebox) blockState);
                    }
                }
            }
        }
    }
}
