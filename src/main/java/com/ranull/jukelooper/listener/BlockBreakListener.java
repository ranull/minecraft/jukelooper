package com.ranull.jukelooper.listener;

import com.ranull.jukelooper.JukeLooper;
import org.bukkit.block.Jukebox;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

public class BlockBreakListener implements Listener {
    private final JukeLooper plugin;

    public BlockBreakListener(JukeLooper plugin) {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onBlockBreak(BlockBreakEvent event) {
        if (event.getBlock().getState() instanceof Jukebox
                && plugin.getLooperManager().hasLooper(event.getBlock().getLocation())) {
            plugin.getLooperManager().removeLooper(plugin.getLooperManager().getLooper(event.getBlock().getLocation()));
        }
    }
}
