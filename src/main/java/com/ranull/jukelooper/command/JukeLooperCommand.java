package com.ranull.jukelooper.command;

import com.ranull.jukelooper.JukeLooper;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class JukeLooperCommand implements CommandExecutor, TabExecutor {
    private final JukeLooper plugin;

    public JukeLooperCommand(com.ranull.jukelooper.JukeLooper plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(@NotNull CommandSender commandSender, @NotNull Command command,
                             @NotNull String string, String[] args) {
        if (args.length == 0) {
            commandSender.sendMessage(ChatColor.GOLD + "♫♪" + ChatColor.DARK_GRAY + " » " + ChatColor.GOLD
                    + "JukeLooper " + ChatColor.DARK_GRAY + "v" + plugin.getDescription().getVersion());
            commandSender.sendMessage(
                    ChatColor.GOLD + "/jukelooper " + ChatColor.DARK_GRAY + "-" + ChatColor.RESET + " Plugin info");

            if (commandSender.hasPermission("jukelooper.reload")) {
                commandSender.sendMessage(ChatColor.GOLD + "/jukelooper reload " + ChatColor.DARK_GRAY + "-" + ChatColor.RESET
                        + " Reload plugin");
            }

            commandSender.sendMessage(ChatColor.DARK_GRAY + "Author: " + ChatColor.GOLD + "Ranull");
        } else if (args[0].equals("reload")) {
            if (commandSender.hasPermission("jukelooper.reload")) {
                plugin.registerConfigHack();
                commandSender.sendMessage(ChatColor.GOLD + "♫♪" + ChatColor.DARK_GRAY + " » "
                        + ChatColor.RESET + "Reloaded config file.");
            } else {
                commandSender.sendMessage(ChatColor.GOLD + "♫♪" + ChatColor.DARK_GRAY + " » "
                        + ChatColor.RESET + "No Permission.");
            }
        }

        return true;
    }

    @Override
    public List<String> onTabComplete(@NotNull CommandSender commandSender, @NotNull Command command,
                                      @NotNull String string, @NotNull String[] args) {
        if (commandSender.hasPermission("jukelooper.reload")) {
            return Collections.singletonList("reload");
        }

        return new ArrayList<>();
    }
}
