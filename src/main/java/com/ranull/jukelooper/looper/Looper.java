package com.ranull.jukelooper.looper;

import org.bukkit.Location;
import org.bukkit.block.Jukebox;

public class Looper {
    private final Location location;
    private long startTime;
    private long duration;

    public Looper(Location location) {
        this.location = location;
        this.startTime = System.currentTimeMillis();
    }

    public Location getLocation() {
        return location;
    }

    public Jukebox getJukebox() {
        return location.getBlock().getState() instanceof Jukebox ? (Jukebox) location.getBlock().getState() : null;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }
}
