package com.ranull.jukelooper.manager;

import com.ranull.jukelooper.JukeLooper;
import com.ranull.jukelooper.looper.Looper;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.*;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.*;

public class LooperManager {
    private final JukeLooper plugin;
    private final Map<Location, Looper> looperMap;
    private final Map<Location, Material> ejectMap;

    public LooperManager(JukeLooper plugin) {
        this.plugin = plugin;
        this.looperMap = new HashMap<>();
        this.ejectMap = new HashMap<>();

        startSecondTimer();
    }

    private void startSecondTimer() {
        new BukkitRunnable() {
            @Override
            public void run() {
                for (Map.Entry<Location, Looper> entry : looperMap.entrySet()) {
                    Looper looper = entry.getValue();

                    if (looper.getDuration() > 0
                            && (looper.getDuration() <= System.currentTimeMillis() - looper.getStartTime())) {
                        new BukkitRunnable() {
                            @Override
                            public void run() {
                                nextDisc(looper);
                            }
                        }.runTask(plugin);
                    }
                }
            }
        }.runTaskTimerAsynchronously(plugin, 0L, 5L);
    }

    public boolean hasLooper(Location location) {
        return looperMap.containsKey(location);
    }

    public Looper createLooper(Jukebox jukebox) {
        Looper looper = new Looper(jukebox.getLocation());

        looperMap.put(jukebox.getLocation(), looper);

        return looper;
    }

    public void removeLooper(Looper looper) {
        looperMap.remove(looper.getLocation());
    }

    public void disable() {
        for (Looper looper : looperMap.values()) {
            Jukebox jukebox = looper.getJukebox();

            if (jukebox != null) {
                try {
                    jukebox.stopPlaying();
                    jukebox.update(true, true);
                } catch (NoSuchMethodError ignored) {
                }
            }
        }
    }

    public void updateLooper(Looper looper) {
        updateLooper(looper, looper.getJukebox().getRecord());
    }

    public void updateLooper(Looper looper, ItemStack itemStack) {
        long duration = getDuration(looper);

        if (duration > 0) {
            looper.setStartTime(System.currentTimeMillis());
            looper.setDuration(duration);
            playDisc(looper.getJukebox(), itemStack);
        }
    }

    public void playDisc(Jukebox jukebox, ItemStack itemStack) {
        Plugin jukeboxExtended = plugin.getServer().getPluginManager().getPlugin("JukeboxExtended");
        Plugin jukeboxExtendedReborn = plugin.getServer().getPluginManager().getPlugin("JukeboxExtendedReborn");

        // JukeboxExtended
        if (jukeboxExtended != null && jukeboxExtended.isEnabled()) {
            try {
                new me.tajam.jext.DiscPlayer(new me.tajam.jext.DiscContainer(itemStack)).play(jukebox.getLocation());

                return;
            } catch (IllegalStateException ignored) {
            }
        }

        // JukeboxExtendedReborn
        if (jukeboxExtendedReborn != null && jukeboxExtendedReborn.isEnabled()) {
            try {
                new me.spartacus04.jext.disc.DiscPlayer(new me.spartacus04.jext.disc.DiscContainer(itemStack))
                        .play(jukebox.getLocation());

                return;
            } catch (IllegalStateException ignored) {
            }
        }

        // Minecraft
        jukebox.setPlaying(itemStack.getType());
    }

    public long getDuration(Looper looper) {
        ItemStack itemStack = looper.getJukebox().getRecord();
        Plugin jukeboxExtended = plugin.getServer().getPluginManager().getPlugin("JukeboxExtended");
        Plugin jukeboxExtendedReborn = plugin.getServer().getPluginManager().getPlugin("JukeboxExtendedReborn");

        // JukeboxExtended
        if (jukeboxExtended != null && jukeboxExtended.isEnabled()) {
            try {
                String namespace = new me.tajam.jext.DiscContainer(itemStack).getNamespace();

                if (plugin.getConfig().isSet("settings/namespace/" + namespace)) {
                    return plugin.getConfig().getLong("settings/namespace/" + namespace) * 1000;
                }
            } catch (IllegalStateException ignored) {
            }
        }

        // JukeboxExtendedReborn
        if (jukeboxExtendedReborn != null && jukeboxExtendedReborn.isEnabled()) {
            try {
                String namespace = new me.spartacus04.jext.disc.DiscContainer(itemStack).getNamespace();

                if (plugin.getConfig().isSet("settings/namespace/" + namespace)) {
                    return plugin.getConfig().getLong("settings/namespace/" + namespace) * 1000;
                }
            } catch (IllegalStateException ignored) {
            }
        }

        // Model Data
        if (itemStack.getItemMeta() != null && itemStack.getItemMeta().hasCustomModelData()
                && plugin.getConfig().isSet("settings/model-data/" + itemStack.getItemMeta().getCustomModelData())) {
            return plugin.getConfig().getLong("settings/model-data/"
                    + itemStack.getItemMeta().getCustomModelData()) * 1000;
        }

        // Minecraft
        if (plugin.getConfig().isSet("settings/material/" + itemStack.getType())) {
            return plugin.getConfig().getLong("settings/material/" + itemStack.getType()) * 1000;
        }

        return -1;
    }

    public Looper getLooper(Location location) {
        return looperMap.get(location);
    }

    public void nextDisc(Looper looper) {
        Jukebox jukebox = looper.getJukebox();
        Block storage = findStorage(looper.getLocation());

        if (jukebox != null && storage != null) {
            if (jukebox.getRecord().getType().isRecord()) {
                Block blockDown = looper.getLocation().getBlock().getRelative(BlockFace.DOWN);
                Plugin jukeboxExtended = plugin.getServer().getPluginManager().getPlugin("JukeboxExtended");
                Plugin JukeboxExtendedReborn = plugin.getServer().getPluginManager().getPlugin("JukeboxExtendedReborn");

                // JukeboxExtended
                if (jukeboxExtended != null && jukeboxExtended.isEnabled()) {
                    try {
                        new me.tajam.jext.DiscPlayer(new me.tajam.jext.DiscContainer(jukebox
                                .getRecord())).stop(jukebox.getLocation());
                    } catch (IllegalStateException ignored) {
                    }
                }

                // JukeboxExtendedReborn
                if (JukeboxExtendedReborn != null && JukeboxExtendedReborn.isEnabled()) {
                    try {
                        new me.spartacus04.jext.disc.DiscPlayer(new me.spartacus04.jext.disc.DiscContainer(jukebox
                                .getRecord())).stop(jukebox.getLocation());
                    } catch (IllegalStateException ignored) {
                    }
                }

                // Storage
                if (blockDown.getState() instanceof Hopper) {
                    Inventory inventory = ((Hopper) blockDown.getState()).getInventory();

                    if (inventory.firstEmpty() != -1) {
                        inventory.addItem(jukebox.getRecord());
                        ejectClearJukebox(jukebox);
                    } else if (putItemInStorage(jukebox.getRecord(), storage.getLocation())) {
                        ejectClearJukebox(jukebox);
                    }
                } else if (putItemInStorage(jukebox.getRecord(), storage.getLocation())) {
                    ejectClearJukebox(jukebox);
                }
            }

            ItemStack recordFromStorage = takeItemFromStorage(storage.getLocation());

            if (recordFromStorage != null) {
                jukebox.setRecord(recordFromStorage);
                jukebox.update(true, true);
                updateLooper(looper);
            } else if (jukebox.getRecord().getType().isRecord()) {
                updateLooper(looper);
            } else {
                removeLooper(looper);
            }
        }
    }

    public void ejectClearJukebox(Jukebox jukebox) {
        ejectMap.put(jukebox.getLocation().clone().add(0.5, 0.5, 0.5), jukebox.getRecord().getType());
        jukebox.setRecord(new ItemStack(Material.AIR));
        jukebox.setPlaying(Material.AIR);
        jukebox.getBlock().setType(Material.JUKEBOX);
        jukebox.eject();
        jukebox.update(true, true);
    }

    public Map<Location, Material> getEjectMap() {
        return ejectMap;
    }

    public boolean putItemInStorage(ItemStack itemStack, Location location) {
        Block block = location.getBlock();

        if (block.getState() instanceof Hopper) {
            ((Hopper) block.getState()).getInventory().addItem(itemStack);

            return true;
        } else if (block.getState() instanceof Chest) {
            ((Chest) block.getState()).getBlockInventory().addItem(itemStack);

            return true;
        }

        return false;
    }

    private ItemStack takeItemFromStorage(Location storage) {
        List<ItemStack> recordList = new ArrayList<>();

        if (storage.getBlock().getState() instanceof Chest) {
            Chest chest = (Chest) storage.getBlock().getState();

            for (ItemStack itemStack : chest.getBlockInventory().getContents()) {
                if (itemStack != null && itemStack.getType().isRecord()) {
                    recordList.add(itemStack);
                }
            }

            if (!recordList.isEmpty()) {
                Collections.shuffle(recordList);
                chest.getBlockInventory().removeItem(recordList.get(0));
            }
        } else if (storage.getBlock().getState() instanceof Hopper) {
            Hopper hopper = (Hopper) storage.getBlock().getState();

            for (ItemStack itemStack : hopper.getInventory().getContents()) {
                if (itemStack != null && itemStack.getType().isRecord()) {
                    recordList.add(itemStack);
                }
            }

            if (!recordList.isEmpty()) {
                Collections.shuffle(recordList);
                hopper.getInventory().removeItem(recordList.get(0));
            }
        }

        return !recordList.isEmpty() ? recordList.get(0) : null;
    }

    public Block findStorage(Location location) {
        BlockState blockState = location.getBlock().getRelative(BlockFace.UP).getState();

        return blockState instanceof Chest || blockState instanceof Hopper ? blockState.getBlock() : null;
    }
}
