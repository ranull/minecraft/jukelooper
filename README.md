<div align="center">
    <img style="margin: auto;" src="https://proxy.spigotmc.org/12d0257c2ff21833cc008b2dc97498c561a9f84b?url=https%3A%2F%2Ffontmeme.com%2Fpermalink%2F191121%2F829c770e97356e986c9d83e71d3fddd8.png"></img>
</div>
<br>
<p>A simple plugin to loop music, setup playlists and change songs!<p>
<b>Inspiration:</b>
<p>This plugin was hugely inspired by JukeLoop. But is a full rewrite with none of the original code.</p>
<b>Usage:</b>
<p>Place a chest over a Jukebox and put your discs in it, once the song ends it will auto-play to a random one in the chest! You can place a button on the chest to skip song.</p>